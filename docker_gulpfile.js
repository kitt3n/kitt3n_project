/**
 * Includes
 */
var gulp = require('gulp');
var sass = require('gulp-sass');
var cleanCSS = require('gulp-clean-css');
var autoprefixer = require('gulp-autoprefixer');
var rename = require('gulp-rename');
var concat = require('gulp-concat');
var uglify = require('gulp-terser');
var wrap = require('gulp-wrap');
var tap = require('gulp-tap');
var util = require('gulp-util');
var debug = require('gulp-debug');
var path = require('path');

var pathToAssets = 'web/typo3conf/ext/**/Resources/Private/Assets/';

var pathToJs = 'Js/';
var pathToJsIncludeJs = 'includeJS/';
var pathToJsIncludeJsLibs = 'includeJSLibs/';
var pathToJsIncludeJsFooter = 'includeJSFooter/';
var pathToJsIncludeJsFooterlibs = 'includeJSFooterlibs/';
var pathToJsHeaderData = 'headerData/';
var pathToJsFooterData = 'footerData/';

var pathToCss = 'Css/';
var pathToScss = 'Scss/';
var pathToCssIncludeCss = 'includeCSS/';
var pathToCssIncludeCssLibs = 'includeCSSLibs/';
var pathToCssHeaderData = 'headerData/';

var pathToTs = 'TypoScript/';

var pathPre = 'pre/';
var pathPost = 'post/';

var pathToCompiledFiles = 'web/typo3conf/ext/kitt3n_custom/Resources/Public/Assets/Main/';

var paths = {
    src: {
        includeCSS: [
            pathToAssets + pathToScss + pathToCssIncludeCss + pathPre + '*.scss',
            pathToAssets + pathToScss + pathToCssIncludeCss + '*.scss',
            pathToAssets + pathToScss + pathToCssIncludeCss + pathPost + '*.scss'
        ],
        includeCSSLibs: [
            pathToAssets + pathToScss + pathToCssIncludeCssLibs + pathPre + '*.scss',
            pathToAssets + pathToScss + pathToCssIncludeCssLibs + '*.scss',
            pathToAssets + pathToScss + pathToCssIncludeCssLibs + pathPost + '*.scss'
        ],
        includeCssHeaderData: [
            pathToAssets + pathToScss + pathToCssHeaderData + pathPre + '*.scss',
            pathToAssets + pathToScss + pathToCssHeaderData + '*.scss',
            pathToAssets + pathToScss + pathToCssHeaderData + pathPost + '*.scss'
        ],
        includeJS: [
            pathToAssets + pathToJs + pathToJsIncludeJs + pathPre + '*.js',
            pathToAssets + pathToJs + pathToJsIncludeJs + '*.js',
            pathToAssets + pathToJs + pathToJsIncludeJs + pathPost + '*.js'
        ],
        includeJSLibs: [
            pathToAssets + pathToJs + pathToJsIncludeJsLibs + pathPre + '*.js',
            pathToAssets + pathToJs + pathToJsIncludeJsLibs + '*.js',
            pathToAssets + pathToJs + pathToJsIncludeJsLibs + pathPost + '*.js'
        ],
        includeJSFooter: [
            pathToAssets + pathToJs + pathToJsIncludeJsFooter + pathPre + '*.js',
            pathToAssets + pathToJs + pathToJsIncludeJsFooter + '*.js',
            pathToAssets + pathToJs + pathToJsIncludeJsFooter + pathPost + '*.js'
        ],
        includeJSFooterlibs: [
            pathToAssets + pathToJs + pathToJsIncludeJsFooterlibs + pathPre + '*.js',
            pathToAssets + pathToJs + pathToJsIncludeJsFooterlibs + '*.js',
            pathToAssets + pathToJs + pathToJsIncludeJsFooterlibs + pathPost + '*.js'
        ],
        includeJSHeaderData: [
            pathToAssets + pathToJs + pathToJsHeaderData + pathPre + '*.js',
            pathToAssets + pathToJs + pathToJsHeaderData + '*.js',
            pathToAssets + pathToJs + pathToJsHeaderData + pathPost + '*.js'
        ],
        includeJSFooterData: [
            pathToAssets + pathToJs + pathToJsFooterData + pathPre + '*.js',
            pathToAssets + pathToJs + pathToJsFooterData + '*.js',
            pathToAssets + pathToJs + pathToJsFooterData + pathPost + '*.js'
        ]
    },
    dest: {
        css: pathToCompiledFiles + pathToCss,
        js: pathToCompiledFiles + pathToJs,
        ts: pathToCompiledFiles + pathToTs,
    }
};
var watcherPaths = {
    src: {
        includeCSS: [
            pathToAssets + pathToScss + pathToCssIncludeCss + '**'
        ],
        includeCSSLibs: [
            pathToAssets + pathToScss + pathToCssIncludeCssLibs + '**'
        ],
        includeCssHeaderData: [
            pathToAssets + pathToScss + pathToCssHeaderData + '**'
        ],
        includeJS: [
            pathToAssets + pathToJs + pathToJsIncludeJs + '**'
        ],
        includeJSLibs: [
            pathToAssets + pathToJs + pathToJsIncludeJsLibs + '**'
        ],
        includeJSFooter: [
            pathToAssets + pathToJs + pathToJsIncludeJsFooter + '**'
        ],
        includeJSFooterlibs: [
            pathToAssets + pathToJs + pathToJsIncludeJsFooterlibs + '**'
        ],
        includeJSHeaderData: [
            pathToAssets + pathToJs + pathToJsHeaderData + '**'
        ],
        includeJSFooterData: [
            pathToAssets + pathToJs + pathToJsFooterData + '**'
        ]
    }
};

/**
 * Scss => Css
 */
gulp.task('includeCSS', function() {
    gulp.src(
        paths.src.includeCSS
    )
        .pipe(concat('main.css'))
        .pipe(sass({
            paths: [ 'web' ]
        }).on('error', sass.logError))
        .pipe(gulp.dest(paths.dest.css))
        .pipe(rename('main.min.css'))
        .pipe(autoprefixer({
            browsers: ['last 3 versions', 'safari 7', 'safari 8']
        }))
        .pipe(cleanCSS(
            {
                level: {
                    2: {
                        // mergeAdjacentRules: false, // controls adjacent rules merging; defaults to true
                        // mergeIntoShorthands: false, // controls merging properties into shorthands; defaults to true
                        mergeMedia: false, // controls `@media` merging; defaults to true
                        mergeNonAdjacentRules: false // controls non-adjacent rule merging; defaults to true
                        // mergeSemantically: true // controls semantic merging; defaults to false
                        // overrideProperties: true, // controls property overriding based on understandability; defaults to true
                        // reduceNonAdjacentRules: true, // controls non-adjacent rule reducing; defaults to true
                        // removeDuplicateFontRules: true, // controls duplicate `@font-face` removing; defaults to true
                        // removeDuplicateMediaBlocks: true, // controls duplicate `@media` removing; defaults to true
                        // removeDuplicateRules: true, // controls duplicate rules removing; defaults to true
                        // restructureRules: true // controls rule restructuring; defaults to false
                    }

                }
            }
        ))
        .pipe(gulp.dest(paths.dest.css))
});
gulp.task('includeCSSLibs', function() {
    gulp.src(
        paths.src.includeCSSLibs
    )
        .pipe(concat('main.libs.css'))
        .pipe(sass({
            paths: [ 'web' ]
        }).on('error', sass.logError))
        .pipe(gulp.dest(paths.dest.css))
        .pipe(rename('main.libs.min.css'))
        .pipe(autoprefixer({
            browsers: ['last 3 versions', 'safari 7', 'safari 8']
        }))
        .pipe(cleanCSS(
            {
                level: {
                    2: {
                        mergeMedia: false, // controls `@media` merging; defaults to true
                        mergeNonAdjacentRules: false // controls non-adjacent rule merging; defaults to true
                    }
                }
            }
        ))
        .pipe(gulp.dest(paths.dest.css))
});

/**
 * Js => Js.min
 */
gulp.task('includeJS', function() {
    return gulp.src(
        paths.src.includeJS
    )
        .pipe(concat('main.head.js'))
        .pipe(gulp.dest(paths.dest.js))
        .pipe(rename('main.head.min.js'))
        .pipe(uglify({compress: {
                comparisons: false,
                conditionals: false
            }}))
        .pipe(gulp.dest(paths.dest.js));
});
gulp.task('includeJSLibs', function() {
    return gulp.src(
        paths.src.includeJSLibs
    )
        .pipe(concat('main.head.libs.js'))
        .pipe(gulp.dest(paths.dest.js))
        .pipe(rename('main.head.libs.min.js'))
        .pipe(uglify({compress: {
                comparisons: false,
                conditionals: false
            }}))
        .pipe(gulp.dest(paths.dest.js));
});
gulp.task('includeJSFooter', function() {
    return gulp.src(
        paths.src.includeJSFooter
    )
        .pipe(concat('main.js'))
        .pipe(gulp.dest(paths.dest.js))
        .pipe(rename('main.min.js'))
        .pipe(uglify({compress: {
                comparisons: false,
                conditionals: false
            }}))
        .pipe(gulp.dest(paths.dest.js));
});
gulp.task('includeJSFooterlibs', function() {
    return gulp.src(
        paths.src.includeJSFooterlibs
    )
        .pipe(concat('main.libs.js'))
        .pipe(gulp.dest(paths.dest.js))
        .pipe(rename('main.libs.min.js'))
        .pipe(uglify({compress: {
                comparisons: false,
                conditionals: false
            }}))
        .pipe(gulp.dest(paths.dest.js));
});

/**
 * TS => Inline Js & Css
 */
gulp.task('includeCssHeaderData', function() {
    return gulp.src(
        paths.src.includeCssHeaderData
    )
        .pipe(concat('main.headerdata.css'))
        .pipe(sass({
            paths: [ 'web' ]
        }).on('error', sass.logError))
        .pipe(rename('css.headerdata.ts'))
        .pipe(autoprefixer({
            browsers: ['last 3 versions', 'safari 7', 'safari 8']
        }))
        .pipe(cleanCSS(
            {
                level: {
                    2: {
                        mergeMedia: false, // controls `@media` merging; defaults to true
                        mergeNonAdjacentRules: false // controls non-adjacent rule merging; defaults to true
                    }
                }
            }
        ))
        .pipe(wrap('page{\nheaderData.333333 = TEXT\nheaderData.333333.value ( \n<%= contents %>\n)\nheaderData.333333.wrap =<style>|</style>\n}\n\n'))
        .pipe(gulp.dest(paths.dest.ts));
});
gulp.task('includeJSHeaderData', function() {
    return gulp.src(
        paths.src.includeJSHeaderData
    )
        .pipe(concat('headerdata.js'))
        .pipe(rename('js.headerdata.ts'))
        .pipe(uglify({compress: {
                comparisons: false,
                conditionals: false
            }}))
        .pipe(wrap('page {\nheaderData.6666666 = TEXT\nheaderData.6666666.value ( \n<%= contents %>\n)\nheaderData.6666666.wrap =<script>|</script>\n}\n\n'))
        .pipe(gulp.dest(paths.dest.ts));
});
gulp.task('includeJSFooterData', function() {
    return gulp.src(
        paths.src.includeJSFooterData
    )
        .pipe(concat('footerdata.js'))
        .pipe(rename('js.footerdata.ts'))
        .pipe(uglify({compress: {
                comparisons: false,
                conditionals: false
            }}))
        .pipe(wrap('page {\nfooterData.9899999 = TEXT\nfooterData.9899999.value ( \n<%= contents %>\n)\nfooterData.9899999.wrap =<script>|</script>\n}\n\n'))
        .pipe(gulp.dest(paths.dest.ts));
});

/**
 * Fly Willy fly ;)
 */
gulp.task(
    'default',
    [
        'includeCSS',
        'includeCSSLibs',
        'includeCssHeaderData',
        'includeJS',
        'includeJSLibs',
        'includeJSFooter',
        'includeJSFooterlibs',
        'includeJSHeaderData',
        'includeJSFooterData'
    ],
    function() {
        gulp.watch(watcherPaths.src.includeCSS, ['includeCSS'])
            .on('change', function(evt) {
                console.log(
                    '[watcher] File ' + evt.path + ' was ' + evt.type + ', compiling...'
                );
            });
        gulp.watch(watcherPaths.src.includeCSSLibs, ['includeCSSLibs'])
            .on('change', function(evt) {
                console.log(
                    '[watcher] File ' + evt.path + ' was ' + evt.type + ', compiling...'
                );
            });
        gulp.watch(watcherPaths.src.includeCssHeaderData, ['includeCssHeaderData'])
            .on('change', function(evt) {
                console.log(
                    '[watcher] File ' + evt.path + ' was ' + evt.type + ', compiling...'
                );
            });
        gulp.watch(watcherPaths.src.includeJS, ['includeJS'])
            .on('change', function(evt) {
                console.log(
                    '[watcher] File ' + evt.path + ' was ' + evt.type + ', compiling...'
                );
            });
        gulp.watch(watcherPaths.src.includeJSLibs, ['includeJSLibs'])
            .on('change', function(evt) {
                console.log(
                    '[watcher] File ' + evt.path + ' was ' + evt.type + ', compiling...'
                );
            });
        gulp.watch(watcherPaths.src.includeJSFooter, ['includeJSFooter'])
            .on('change', function(evt) {
                console.log(
                    '[watcher] File ' + evt.path + ' was ' + evt.type + ', compiling...'
                );
            });
        gulp.watch(watcherPaths.src.includeJSFooterlibs, ['includeJSFooterlibs'])
            .on('change', function(evt) {
                console.log(
                    '[watcher] File ' + evt.path + ' was ' + evt.type + ', compiling...'
                );
            });
        gulp.watch(watcherPaths.src.includeJSHeaderData, ['includeJSHeaderData'])
            .on('change', function(evt) {
                console.log(
                    '[watcher] File ' + evt.path + ' was ' + evt.type + ', compiling...'
                );
            });
        gulp.watch(watcherPaths.src.includeJSFooterData, ['includeJSFooterData'])
            .on('change', function(evt) {
                console.log(
                    '[watcher] File ' + evt.path + ' was ' + evt.type + ', compiling...'
                );
            });
    }
);