Please copy production_x.ini. Then change content.

```
cp production_x.ini production_1.ini
cp production_x.ini production_2.ini
cp production_x.ini production_3.ini
cp production_x.ini production_4.ini
cp production_x.ini production_5.ini
```

Please copy sudo_dummy.ini. Then change content. Do _not_ use "$" in password.

```
cp sudo_dummy.ini sudo.ini
```
