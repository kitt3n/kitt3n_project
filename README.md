![version 9.5.0](https://img.shields.io/badge/version-9.5.0-orange.svg?style=flat-square)
   ![license MIT](https://img.shields.io/badge/license-MIT-green.svg?style=flat-square)

```
NAME
    kitt3n_project

SYNOPSIS
    Good starting point for a TYPO3 project

DESCRIPTION
    Install TYPO3 in a preconfigured docker machine. Deploy your project via bitbucket piplines.

REQUIREMENTS
    Git
    Docker (for Mac) [https://www.docker.com/]
    Docker Compose [https://docs.docker.com/compose/]
    Docker Sync (on Mac) [http://docker-sync.io/]
    Bash  

INSTALLATION 

    On Bitbucket:

        Create a (private) project and fork 'kitt3n_project' and 'kitt3n_custom' into the project.

        Name the repos 'kitt3n_project' and '<customer_abbr>_<yy>_<type>_thm'

        Create a branch 'custom' in '<customer_abbr>_<yy>_<type>_thm' from most recent 'release/X.Y' branch.
    
    On your computer:

        Creat a project with your preferred IDE.

        Clone the fork of 'kitt3n_project' into your project.

        Edit '.env' and replace placeholders with a unique value. Otherwise docker would override containers from multiple projects with each other.

        Edit 'composer.json' and replace placeholder with '<customer_abbr>_<yy>_<type>_thm'.

        Start docker (e.g. via 'sudo systemctl start docker.service' on arch linux)

        Run 'sudo make up-on-linux' or 'make up-on-osx' to build the docker containers.

        Change into the built app container via 'sudo make root-on-linux' or make 'root-on-osx'.

            Create a '.ssh' folder in '/root/'

            Copy your private and public key (which is configured in your bitbucket account) to '/root/.ssh/'

            Creat a config in '/root/.ssh/' which contains at least:

                Host bitbucket.org
                    IdentityFile ~/.ssh/your_private_key

            Run 'chmod -R 600 /root/.ssh/'

            Run 'sh /app/docker_install_typo3.sh'.

            Run 'mysql -hmysql dev -udev -pdev < /app/171009_dummy.sql'

            Optional run '-hmysql dev -udev -pdev < /app/171009_guest.sql'

            Optional run 'chmod -R 777 /app/web/'

        Change into built phpgulp container via 'sudo make gulp-on-linux' or 'make gulp-on-osx'.

            Run 'gulp --gulpfile=docker_gulpfile.js'.


        ENJOY a ready to use TYPO3 installation :)
```