/**
 * Includes
 */
var gulp = require('gulp');
var sass = require('gulp-sass');
var cleanCSS = require('gulp-clean-css');
var autoprefixer = require('gulp-autoprefixer');
var rename = require('gulp-rename');
var concat = require('gulp-concat');
var uglify = require('gulp-terser');
var wrap = require('gulp-wrap');
var tap = require('gulp-tap');
var util = require('gulp-util');
var debug = require('gulp-debug');
var path = require('path');

var pathToAssets = 'web/typo3conf/ext/**/Resources/Private/Assets/';

var pathToJs = 'Js/';
var pathToJsIncludeJs = 'includeJS/';
var pathToJsIncludeJsLibs = 'includeJSLibs/';
var pathToJsIncludeJsFooter = 'includeJSFooter/';
var pathToJsIncludeJsFooterlibs = 'includeJSFooterlibs/';
var pathToJsHeaderData = 'headerData/';
var pathToJsFooterData = 'footerData/';

var pathToCss = 'Css/';
var pathToScss = 'Scss/';
var pathToCssIncludeCss = 'includeCSS/';
var pathToCssIncludeCssLibs = 'includeCSSLibs/';
var pathToCssHeaderData = 'headerData/';

var pathPre = 'pre/';
var pathPost = 'post/';

var pathToCompiledFiles = 'web/typo3conf/ext/kitt3n_custom/Resources/Public/Assets/Main/';

var paths = {
    src: {
        includeCSS: [
            pathToAssets + pathToScss + pathToCssIncludeCss + pathPre + '*.scss',
            pathToAssets + pathToScss + pathToCssIncludeCss + '*.scss',
            pathToAssets + pathToScss + pathToCssIncludeCss + pathPost + '*.scss'
        ],
        includeCSSLibs: [
            pathToAssets + pathToScss + pathToCssIncludeCssLibs + pathPre + '*.scss',
            pathToAssets + pathToScss + pathToCssIncludeCssLibs + '*.scss',
            pathToAssets + pathToScss + pathToCssIncludeCssLibs + pathPost + '*.scss'
        ],
        includeCssHeaderData: [
            pathToAssets + pathToScss + pathToCssHeaderData + pathPre + '*.scss',
            pathToAssets + pathToScss + pathToCssHeaderData + '*.scss',
            pathToAssets + pathToScss + pathToCssHeaderData + pathPost + '*.scss'
        ],
        includeJS: [
            pathToAssets + pathToJs + pathToJsIncludeJs + pathPre + '*.js',
            pathToAssets + pathToJs + pathToJsIncludeJs + '*.js',
            pathToAssets + pathToJs + pathToJsIncludeJs + pathPost + '*.js'
        ],
        includeJSLibs: [
            pathToAssets + pathToJs + pathToJsIncludeJsLibs + pathPre + '*.js',
            pathToAssets + pathToJs + pathToJsIncludeJsLibs + '*.js',
            pathToAssets + pathToJs + pathToJsIncludeJsLibs + pathPost + '*.js'
        ],
        includeJSFooter: [
            pathToAssets + pathToJs + pathToJsIncludeJsFooter + pathPre + '*.js',
            pathToAssets + pathToJs + pathToJsIncludeJsFooter + '*.js',
            pathToAssets + pathToJs + pathToJsIncludeJsFooter + pathPost + '*.js'
        ],
        includeJSFooterlibs: [
            pathToAssets + pathToJs + pathToJsIncludeJsFooterlibs + pathPre + '*.js',
            pathToAssets + pathToJs + pathToJsIncludeJsFooterlibs + '*.js',
            pathToAssets + pathToJs + pathToJsIncludeJsFooterlibs + pathPost + '*.js'
        ],
        includeJSHeaderData: [
            pathToAssets + pathToJs + pathToJsHeaderData + pathPre + '*.js',
            pathToAssets + pathToJs + pathToJsHeaderData + '*.js',
            pathToAssets + pathToJs + pathToJsHeaderData + pathPost + '*.js'
        ],
        includeJSFooterData: [
            pathToAssets + pathToJs + pathToJsFooterData + pathPre + '*.js',
            pathToAssets + pathToJs + pathToJsFooterData + '*.js',
            pathToAssets + pathToJs + pathToJsFooterData + pathPost + '*.js'
        ]
    },
    dest: {
        includeCSS: pathToCompiledFiles + pathToCss,
        includeCSSLibs: pathToCompiledFiles + pathToCss,
        includeCssHeaderData: pathToCompiledFiles + pathToCss,

        includeJS: pathToCompiledFiles + pathToJs,
        includeJSLibs: pathToCompiledFiles + pathToJs,
        includeJSFooter: pathToCompiledFiles + pathToJs,
        includeJSFooterlibs: pathToCompiledFiles + pathToJs,
        includeJSHeaderData: pathToCompiledFiles + pathToJs,
        includeJSFooterData: pathToCompiledFiles + pathToJs
    }
};
var watcherPaths = {
    src: {
        includeCSS: [
            paths.src.includeCSS[0],
            paths.src.includeCSS[1],
            paths.src.includeCSS[2]
        ],
        includeCSSLibs: [
            paths.src.includeCSSLibs[0],
            paths.src.includeCSSLibs[1],
            paths.src.includeCSSLibs[2]
        ]
    }
};

/**
 * Scss => Css
 */
gulp.task('includeCSS', function() {
    gulp.src(
        paths.src.includeCSS
    )
        .pipe(concat('main.css'))
        .pipe(sass({
            paths: [ 'web' ]
        }).on('error', sass.logError))
        .pipe(rename('main.min.css'))
        .pipe(autoprefixer({
            browsers: ['last 3 versions', 'safari 7', 'safari 8']
        }))
        .pipe(cleanCSS(
            {
                level: {
                    2: {
                        // mergeAdjacentRules: false, // controls adjacent rules merging; defaults to true
                        // mergeIntoShorthands: false, // controls merging properties into shorthands; defaults to true
                        mergeMedia: false, // controls `@media` merging; defaults to true
                        mergeNonAdjacentRules: false // controls non-adjacent rule merging; defaults to true
                        // mergeSemantically: true // controls semantic merging; defaults to false
                        // overrideProperties: true, // controls property overriding based on understandability; defaults to true
                        // reduceNonAdjacentRules: true, // controls non-adjacent rule reducing; defaults to true
                        // removeDuplicateFontRules: true, // controls duplicate `@font-face` removing; defaults to true
                        // removeDuplicateMediaBlocks: true, // controls duplicate `@media` removing; defaults to true
                        // removeDuplicateRules: true, // controls duplicate rules removing; defaults to true
                        // restructureRules: true // controls rule restructuring; defaults to false
                    }

                }
            }
        ))
        .pipe(gulp.dest(paths.dest.includeCSS))
});
gulp.task('includeCSSLibs', function() {
    gulp.src(
        paths.src.includeCSSLibs
    )
        .pipe(concat('main.libs.css'))
        .pipe(sass({
            paths: [ 'web' ]
        }).on('error', sass.logError))
        .pipe(rename('main.libs.min.css'))
        .pipe(autoprefixer({
            browsers: ['last 3 versions', 'safari 7', 'safari 8']
        }))
        .pipe(cleanCSS(
            {
                level: {
                    2: {
                        mergeMedia: false, // controls `@media` merging; defaults to true
                        mergeNonAdjacentRules: false // controls non-adjacent rule merging; defaults to true
                    }
                }
            }
        ))
        .pipe(gulp.dest(paths.dest.includeCSSLibs))
});
gulp.task('includeCssHeaderData', function() {
    return gulp.src(
        paths.src.includeCssHeaderData
    )
        .pipe(sass())
        .pipe(rename('main.headerdata.min.css'))
        .pipe(autoprefixer({
            browsers: ['last 3 versions', 'safari 7', 'safari 8']
        }))
        .pipe(cleanCSS(
            {
                level: {
                    2: {
                        mergeMedia: false, // controls `@media` merging; defaults to true
                        mergeNonAdjacentRules: false // controls non-adjacent rule merging; defaults to true
                    }
                }
            }
        ))
        .pipe(wrap('page{\nheaderData.333333 = TEXT\nheaderData.333333.value ( \n<%= contents %>\n)\nheaderData.333333.wrap =<style>|</style>\n}\n\n'))
        .pipe(gulp.dest(paths.dest.includeCssHeaderData));
});

/**
 * Js => Js.min
 */
gulp.task('includeJS', function() {
    return gulp.src(
        paths.src.includeJS
    )
        .pipe(concat('main.head.js'))
        .pipe(gulp.dest(paths.dest.includeJS))
        .pipe(rename('main.head.min.js'))
        .pipe(uglify({compress: {
                comparisons: false,
                conditionals: false
            }}))
        .pipe(gulp.dest(paths.dest.includeJS));
});
gulp.task('includeJSLibs', function() {
    return gulp.src(
        paths.src.includeJSLibs
    )
        .pipe(concat('main.head.libs.js'))
        .pipe(gulp.dest(paths.dest.includeJSLibs))
        .pipe(rename('main.head.libs.min.js'))
        .pipe(uglify({compress: {
                comparisons: false,
                conditionals: false
            }}))
        .pipe(gulp.dest(paths.dest.includeJSLibs));
});
gulp.task('includeJSFooterlibs', function() {
    return gulp.src(
        paths.src.includeJSFooterlibs
    )
        .pipe(concat('main.footer.libs.js'))
        .pipe(gulp.dest(paths.dest.includeJSFooterlibs))
        .pipe(rename('main.footer.libs.min.js'))
        .pipe(uglify({compress: {
                comparisons: false,
                conditionals: false
            }}))
        .pipe(gulp.dest(paths.dest.includeJSFooterlibs));
});
gulp.task('includeJSFooter', function() {
    return gulp.src(
        paths.src.includeJSFooter
    )
        .pipe(concat('main.js'))
        .pipe(gulp.dest(paths.dest.includeJSFooter))
        .pipe(rename('main.min.js'))
        .pipe(uglify({compress: {
                comparisons: false,
                conditionals: false
            }}))
        .pipe(gulp.dest(paths.dest.includeJSFooter));
});
gulp.task('includeJSHeaderData', function() {
    return gulp.src(
        paths.src.includeJSHeaderData
    )
        .pipe(concat('headerData.js'))
        .pipe(gulp.dest(paths.dest.includeJSHeaderData))
        .pipe(rename('headerData.ts'))
        .pipe(uglify({compress: {
                comparisons: false,
                conditionals: false
            }}))
        .pipe(wrap('page {\nheaderData.6666666 = TEXT\nheaderData.6666666.value ( \n<%= contents %>\n)\nheaderData.6666666.wrap =<script>|</script>\n}\n\n'))
        .pipe(gulp.dest(paths.dest.includeJSHeaderData));
});

gulp.task('includeJSFooterData', function() {
    return gulp.src(
        paths.src.includeJSFooterData
    )
        .pipe(concat('footerData.js'))
        .pipe(gulp.dest(paths.dest.includeJSFooterData))
        .pipe(rename('footerData.ts'))
        .pipe(uglify({compress: {
                comparisons: false,
                conditionals: false
            }}))
        .pipe(wrap('page {\nfooterData.9999999 = TEXT\nfooterData.9999999.value ( \n<%= contents %>\n)\nfooterData.9999999.wrap =<script>|</script>\n}\n\n'))
        .pipe(gulp.dest(paths.dest.includeJSFooterData));
});

/**
 * Fly Willy fly ;)
 */
gulp.task(
    'default',
    [
        'includeCSS',
        'includeCSSLibs',
        'includeCssHeaderData',
        'includeJS',
        'includeJSLibs',
        'includeJSFooter',
        'includeJSFooterlibs',
        'includeJSHeaderData',
        'includeJSFooterData'
    ],
    function() {
    }
);