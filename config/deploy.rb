# config valid only for current version of Capistrano
lock '3.5.0'

set :application, "<customer_abbr>_<type>_<yy>-deploy"
##
## :repo_url
##
## e.g. set :repo_url, 'git@bitbucket.org:zwei14/z14_ws_18_project.git'
##
set :repo_url, "git@bitbucket.org:<team>/<customer_abbr>_<type>_<yy>_project.git"
set :branch, ENV['REVISION'] || ENV['BRANCH_NAME']
set :keep_releases, 1

####################################################################################################################
##
## Mittwald:
##
# Rake::Task["deploy:check"].clear_actions
##
####################################################################################################################

namespace :deploy do
  ####################################################################################################################
  ##
  ## Mittwald:
  ##
  # task check: :'git:wrapper' do
  #   on release_roles :all do
  #     ####################################################################################################################
  #     ##
  #     ## /usr/bin/ssh not present at mittwald servers
  #     ## Use /usr/local/bin/ssh instead 
  #     ##
  #     execute :mkdir, "-p", "#{fetch(:tmp_dir)}/#{fetch(:application)}/"
  #     upload! StringIO.new("#!/bin/sh -e\nexec /usr/local/bin/ssh -o PasswordAuthentication=no -o StrictHostKeyChecking=no \"$@\"\n"), "#{fetch(:tmp_dir)}/#{fetch(:application)}/git-ssh.sh"
  #     execute :chmod, "+x", "#{fetch(:tmp_dir)}/#{fetch(:application)}/git-ssh.sh"
  #     ##
  #     ####################################################################################################################
  #   end
  # end
  ##
  ####################################################################################################################
  task :composer do
    on roles(:web) do
      within release_path do
        execute :chmod, '-R', '775', './web'
        execute :chmod, '-R', '775', './composer.json'
        execute "echo '#{fetch(:kitt3n_database_ini)}'"

        ####################################################################################################################
        ##
        ## DomainFactory:
        ##        
        execute 'wget', '-q', '-nc', 'https://getcomposer.org/composer.phar'
        execute "#{fetch(:kitt3n_php_cli_bin)}", "-c", "./php.ini" , './composer.phar', 'install', '--no-dev', '--no-ansi', '--no-interaction', '--optimize-autoloader'
        ##
        ## Mittwald:
        # execute :composer, 'install', '--no-dev', '--no-ansi', '--no-interaction'
        ##
        ####################################################################################################################        

        ####################################################################################################################
        ##
        ## Uncomment if not using PHP with CGI !!!
        ##
        #execute :mv, "./web/index.php", "./web/_index.php"
        #execute :cp, "./vendor/typo3/cms/index.php", "./web"
        ##
        ####################################################################################################################

        execute :ln, "-s", "#{fetch(:kitt3n_database_ini)}", "./web/typo3conf/server.ini"
        execute :ln, "-s", "#{fetch(:kitt3n_sudo_ini)}", "./sudo.ini"

        ####################################################################################################################
        ##
        ## Add shared folders below !!!
        ##
        execute :ln, "-s", "#{fetch(:kitt3n_fileadmin)}", "./web/fileadmin"
        execute :ln, "-s", "#{fetch(:kitt3n_uploads)}", "./web/uploads"
        ##
        ## Mittwald (additional):
        ## 
        execute :ln, "-s", "#{fetch(:kitt3n_gulp)}", "./web/typo3conf/ext/#{fetch(:kitt3n_custom_extension)}/Resources/Public/Assets/Main"
        ##
        ####################################################################################################################

        execute :sh, "./install_typo3.sh", "#{fetch(:kitt3n_php_cli_bin)}"

        # ./temp_defaults-extra-file.cnf created has been created in ./install_typo3.sh
        execute :mysql, "--defaults-extra-file=./temp_defaults-extra-file.cnf", "--default-character-set=utf8", "<", "#{fetch(:kitt3n_database_dump_definition)}"
        execute :mysql, "--defaults-extra-file=./temp_defaults-extra-file.cnf", "--default-character-set=utf8", "<", "#{fetch(:kitt3n_database_dump_data)}"

        execute :sh, "./install_typo3_extensions.sh", "#{fetch(:kitt3n_php_cli_bin)}"

        ####################################################################################################################
        ##
        ## Uncomment if hosting at Mittwald !!!
        ##
        execute :sh, "./install_npm_packages.sh"
        execute "#{fetch(:kitt3n_node_modules_bin)}/gulp"
        ##
        ####################################################################################################################

        execute :rm, "-f", "./temp_defaults-extra-file.cnf"

        ####################################################################################################################
        ##
        ## Uncomment if using PHP with opcache enabled !!!
        ##
        #execute "#{fetch(:kitt3n_php_bin)}", "-c", "./php.ini" , './opcache_reset.php'
        ##
        ####################################################################################################################

        execute :touch, "./web/typo3conf/ENABLE_INSTALL_TOOL"
      end
    end
  end
  after :updated, 'deploy:composer'
end
