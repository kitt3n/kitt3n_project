server '138.201.205.25', user: 'ssh-<user>', roles: %w{web}, ssh_options: { forward_agent: true }

##
## :kitt3n_staging_date
##
## e.g. set :kitt3n_staging_date, "<yyyymmddhhmm>"
##
set :kitt3n_staging_date, "<yyyymmddhhmm>"

##
## :kitt3n_db_ini
##
## e.g. set :kitt3n_db_ini, "staging_1.ini"
##
set :kitt3n_db_ini, "staging_1.ini"

##
## :kitt3n_customer_abbr
##
## Customer Abbr.
## e.g. set :kitt3n_customer_abbr, "z14"
##
set :kitt3n_customer_abbr, "<customer_abbr>"

##
## :kitt3n_type
##
## e.g. set :kitt3n_type, "ws"
##
set :kitt3n_type, "ws"

##
## :kitt3n_year
##
## e.g. set :kitt3n_year, "19"
##
set :kitt3n_year, "19"

##
## :kitt3n_stage
##
## e.g. set :kitt3n_stage, "staging"
##
set :kitt3n_stage, "staging"

##
## :kitt3n_custom_extension
##
## e.g. set :kitt3n_custom_extension, "kitt3n_custom"
##
set :kitt3n_custom_extension, "kitt3n_custom"

##
## :kitt3n_node_modules_bin
##
## Absolute path to node_modules
## e.g. set :kitt3n_node_modules_bin, "./node_modules/.bin"
##
set :kitt3n_node_modules_bin, "./node_modules/.bin"

##
## :kitt3n_php_cli_bin
##
## Absolute path to PHP CLI
##
## DomainFactory:
## e.g. set :kitt3n_php_cli_bin, "/usr/local/bin/php7-70STABLE-CLI"
##
## Mittwald:
## e.g. set :kitt3n_php_cli_bin, "/usr/local/bin/php_cli"
##
set :kitt3n_php_cli_bin, "/opt/plesk/php/7.2/bin/php"

##
## :kitt3n_php_bin
##
## Absolute path to PHP
##
## DomainFactory:
## e.g. set :kitt3n_php_bin, "/usr/local/bin/php7-70STABLE-STANDARD"
## e.g. set :kitt3n_php_bin, "/usr/local/bin/php7-70STABLE-FCGI"
##
## Mittwald:
## e.g. set :kitt3n_php_bin, "/usr/local/bin/php"
##
set :kitt3n_php_bin, "/opt/plesk/php/7.2/bin/php"

##
## :kitt3n_server_path
##
## Absolute server path
##
## DomainFactory:
## e.g. set :kitt3n_server_path, "/www/470890_78628/webseiten"
##
## Mittwald:
## e.g. set :kitt3n_server_path, "/home/www/p123456/html/DEV"
##
set :kitt3n_server_path, "/var/www/vhosts/<customer_subscription>/httpdocs"

##
## :kitt3n_quota
##
## Absolute path to quota
## e.g. set :kitt3n_quota, "#{fetch(:kitt3n_server_path)}/#{fetch(:kitt3n_customer_abbr)}#{fetch(:kitt3n_type)}"
##
set :kitt3n_quota, "#{fetch(:kitt3n_server_path)}/#{fetch(:kitt3n_customer_abbr)}/#{fetch(:kitt3n_type)}"

##
## :kitt3n_fileadmin
## :kitt3n_uploads
## 
## Absolute paths to shared folders (e.g. fileadmin, uploads,...) 
##
## set :kitt3n_fileadmin, "#{fetch(:kitt3n_quota)}/#{fetch(:kitt3n_year)}/#{fetch(:kitt3n_stage)}/shared/fileadmin/#{fetch(:kitt3n_staging_date)}"
## set :kitt3n_uploads, "#{fetch(:kitt3n_quota)}/#{fetch(:kitt3n_year)}/#{fetch(:kitt3n_stage)}/shared/uploads/#{fetch(:kitt3n_staging_date)}"
##
## IMPORTANT !!!
## Add shared folders in production.rb and ../deploy.rb
##
set :kitt3n_gulp, "#{fetch(:kitt3n_quota)}/#{fetch(:kitt3n_year)}/#{fetch(:kitt3n_stage)}/shared/gulp/#{fetch(:kitt3n_staging_date)}"
set :kitt3n_fileadmin, "#{fetch(:kitt3n_quota)}/#{fetch(:kitt3n_year)}/#{fetch(:kitt3n_stage)}/shared/fileadmin/#{fetch(:kitt3n_staging_date)}" 
set :kitt3n_uploads, "#{fetch(:kitt3n_quota)}/#{fetch(:kitt3n_year)}/#{fetch(:kitt3n_stage)}/shared/uploads/#{fetch(:kitt3n_staging_date)}"

################################################################################################################################
################################################################################################################################
##
## Do not edit anything below or clowns will eat you
##
################################################################################################################################
################################################################################################################################

##
## :deploy_to
##
## Absolute path to quota + year and staging folder
## e.g. set :deploy_to, "#{fetch(:kitt3n_quota)}/#{fetch(:kitt3n_year)}/staging"
##
set :deploy_to, "#{fetch(:kitt3n_quota)}/#{fetch(:kitt3n_year)}/#{fetch(:kitt3n_stage)}"

##
## :kitt3n_config
## :kitt3n_database_ini
## :kitt3n_sudo_ini
## :kitt3n_database_dump_definition
## :kitt3n_database_dump_data
##
## Absolute path to shared config / ini / dump
## e.g. set :kitt3n_config, "#{fetch(:kitt3n_quota)}/#{fetch(:kitt3n_year)}/#{fetch(:kitt3n_stage)}/shared/config"
## e.g. set :kitt3n_database_ini, "#{fetch(:kitt3n_quota)}/#{fetch(:kitt3n_year)}/#{fetch(:kitt3n_stage)}/shared/config/#{fetch(:kitt3n_db_ini)}"
## e.g. set :kitt3n_sudo_ini, "#{fetch(:kitt3n_quota)}/#{fetch(:kitt3n_year)}/#{fetch(:kitt3n_stage)}/shared/config/sudo.ini"
## e.g. set :kitt3n_database_dump_definition, "#{fetch(:kitt3n_quota)}/#{fetch(:kitt3n_year)}/#{fetch(:kitt3n_stage)}/shared/dump/#{fetch(:kitt3n_staging_date)}/definition.sql"
## e.g. set :kitt3n_database_dump_data, "#{fetch(:kitt3n_quota)}/#{fetch(:kitt3n_year)}/#{fetch(:kitt3n_stage)}/shared/dump/#{fetch(:kitt3n_staging_date)}/data.sql"
##
set :kitt3n_config, "#{fetch(:kitt3n_quota)}/#{fetch(:kitt3n_year)}/#{fetch(:kitt3n_stage)}/shared/config" 
set :kitt3n_database_ini, "#{fetch(:kitt3n_quota)}/#{fetch(:kitt3n_year)}/#{fetch(:kitt3n_stage)}/shared/config/#{fetch(:kitt3n_db_ini)}"
set :kitt3n_sudo_ini, "#{fetch(:kitt3n_quota)}/#{fetch(:kitt3n_year)}/#{fetch(:kitt3n_stage)}/shared/config/sudo.ini"
set :kitt3n_database_dump_definition, "#{fetch(:kitt3n_quota)}/#{fetch(:kitt3n_year)}/#{fetch(:kitt3n_stage)}/shared/dump/#{fetch(:kitt3n_staging_date)}/definition.sql"
set :kitt3n_database_dump_data, "#{fetch(:kitt3n_quota)}/#{fetch(:kitt3n_year)}/#{fetch(:kitt3n_stage)}/shared/dump/#{fetch(:kitt3n_staging_date)}/data.sql"

namespace :staging do

  task :kitt3n_staging do
    on roles(:web) do
      # nothing right now
    end
  end

  after 'deploy:finishing', 'staging:kitt3n_staging'
end