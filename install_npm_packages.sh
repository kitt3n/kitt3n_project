#!bash

npm install gulp \
&& npm install --save-dev gulp \
&& npm install node-sass \
&& npm install gulp-sass --save-dev \
&& npm install gulp-clean-css --save-dev \
&& npm install --save-dev gulp-autoprefixer \
&& npm install --save-dev gulp-wrap \
&& npm install --save-dev gulp-tap \
&& npm install --save-dev gulp-file-include \
&& npm install gulp-concat gulp-rename gulp-uglify --save-dev \
&& npm install --save-dev gulp-util \
&& npm install --save-dev gulp-debug
